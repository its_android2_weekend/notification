package com.example.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.notification.notification.App;
import com.example.notification.notification.DetailActivity;
import static com.example.notification.notification.App.CHANNEL_1_ID;
import static com.example.notification.notification.App.CHANNEL_2_ID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnSimpleNotification;
    Button btnChannel2;
    Button btnNotificationStyle;
    Button btnBigPictureStyle;

    private NotificationManagerCompat managerCompat;//Global;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSimpleNotification = findViewById(R.id.btnSimpleNotification);
        btnChannel2 = findViewById(R.id.btnChannel2);
        btnNotificationStyle = findViewById(R.id.btnNotificationInboxStyle);
        btnBigPictureStyle = findViewById(R.id.btnBigPictureStyle);

        btnSimpleNotification.setOnClickListener(this);
        btnChannel2.setOnClickListener(this);
        btnNotificationStyle.setOnClickListener(this);
        btnBigPictureStyle.setOnClickListener(this);

        //Init: OnCreate();
        managerCompat = NotificationManagerCompat.from(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSimpleNotification:
                sendNotificationChannel1();
                break;
            case R.id.btnChannel2:
                sendNotificationChannel2();
                break;
            case R.id.btnNotificationInboxStyle:
                sendNotificationUsingInboxStyle();
                break;
            case R.id.btnBigPictureStyle:
                sendNotificationBigPictureStyle();
                break;
        }
    }

    private void sendNotificationChannel1() {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("id", "001");
        intent.putExtra("name", "Jonh Cena");
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification1 = new NotificationCompat.Builder(this, App.CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_time_to_leave_black_24dp)
                .setContentTitle("Hello Notification Channel 1")
                .setContentText("Context Text 1")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.fb))
                .setPriority(NotificationCompat.PRIORITY_HIGH) //Version Lower than 8.0
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true) //clear notification
                .build();//Return Notification
        int id = (int) (System.currentTimeMillis() / 1000);
        managerCompat.notify(id, notification1);

        //Testing;
       NotificationCompat.Builder builder = new NotificationCompat.Builder(this, App.CHANNEL_1_ID)
               .setSmallIcon(R.drawable.ic_time_to_leave_black_24dp);



    }

    private void sendNotificationChannel2() {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("id", "001");
        intent.putExtra("name", "Jonh Cena");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification2 = new NotificationCompat.Builder(this, CHANNEL_2_ID)
                .setSmallIcon(R.drawable.ic_time_to_leave_black_24dp)
                .setContentTitle("Hello Notification Channel 2")
                .setContentText("Context Text 2")
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        int id = (int) (System.currentTimeMillis() / 1000);
        managerCompat.notify(id, notification2);
    }

    private void sendNotificationBigPictureStyle() {

        //Pending Intent;
        Intent intent = new Intent(this,DetailActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
        bigPicStyle.bigPicture(BitmapFactory.decodeResource(getResources(),R.drawable.img_bg));
        bigPicStyle.bigLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.fb));
        bigPicStyle.setBigContentTitle("New Promotion From Samsung");
        bigPicStyle.setSummaryText("Buy Samsung Note 10+ today");

        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.synthetic);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_time_to_leave_black_24dp)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setStyle(bigPicStyle)
                .setContentTitle("New Promotion From Samsung") //For Android L
                .setContentText("Buy Samsung Note 10+ today") //For Android L
                .setSound(sound)//for android Lower than 8.0
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();
        int id = (int) (System.currentTimeMillis() / 1000);
        managerCompat.notify(id, notification);
    }

    private void sendNotificationUsingInboxStyle() {
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        String[] events = new String[3];
        events[0] = "1) Message for implicit intent";
        events[1] = "2) big view Notification";
        events[2] = "3) from ITS!";

        // Sets a title for the Inbox style big view
        inboxStyle.setBigContentTitle("More Details:");
        // Moves events into the big view
        for (int i=0; i < events.length; i++) {
            inboxStyle.addLine(events[i]);
        }
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_time_to_leave_black_24dp)
                .setContentTitle("Hello Notification Channel 1")
                .setContentText("Context Text 1")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setStyle(inboxStyle)
                .build();
        int id = (int) (System.currentTimeMillis() / 1000);
        managerCompat.notify(id, notification);
    }
}
