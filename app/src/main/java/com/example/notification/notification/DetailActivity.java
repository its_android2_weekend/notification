package com.example.notification.notification;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.notification.R;

public class DetailActivity extends AppCompatActivity {
    TextView tvdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        tvdata = findViewById(R.id.tvdata);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String strName = bundle.getString("name");
            tvdata.setText(strName);
        }else
            Toast.makeText(this, "Tddd", Toast.LENGTH_SHORT).show();
    }
}
